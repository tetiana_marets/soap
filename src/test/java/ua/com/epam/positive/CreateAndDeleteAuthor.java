package ua.com.epam.positive;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ua.com.epam.entities.*;
import ua.com.epam.services.AuthorService;

public class CreateAndDeleteAuthor {
    AuthorService authorService;

    @BeforeTest
    public void setUp(){
        authorService = new AuthorService();
    }

    @Test
    public void createAndDeleteAuthor (){
        AuthorType authorEntity = authorService.createAuthorEntity();
        CreateAuthorRequest postRequest = new CreateAuthorRequest();
        postRequest.setAuthor(authorEntity);
        AuthorType authorResponse = authorService.createAuthor(postRequest);
        Assert.assertEquals(authorResponse.getAuthorId(),authorEntity.getAuthorId(),"ID's for authors are " +
                "not correct");

        DeleteAuthorRequest deleteRequest = new DeleteAuthorRequest();
        deleteRequest.setAuthorId(authorResponse.getAuthorId());
        String deleteResponse = authorService.deleteAuthor(deleteRequest);
        Assert.assertEquals(deleteResponse, "Successfully deleted author " + authorResponse.getAuthorId(),
                "Author's ID for deleting is incorrect");
    }
}
