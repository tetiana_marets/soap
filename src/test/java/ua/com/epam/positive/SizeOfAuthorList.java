package ua.com.epam.positive;

import org.testng.Assert;
import org.testng.annotations.Test;
import ua.com.epam.constants.Constants;
import ua.com.epam.entities.*;

import java.util.List;

public class SizeOfAuthorList {
    @Test
    public void sizeOfAuthorList () {
        LibraryPortService libraryPortService = new LibraryPortService();
        LibraryPort libraryPort = libraryPortService.getLibraryPortSoap11();

        GetAuthorsRequest getAuthors = new GetAuthorsRequest();
        SearchParamsWithPagination params = new SearchParamsWithPagination(Constants.orderType,Constants.page,
                Constants.pagination,Constants.size);
        getAuthors.setSearch(params);

        GetAuthorsResponse response = libraryPort.getAuthors(getAuthors);
        List<AuthorType> authorTypeList = response.getAuthors().getAuthor();
        Assert.assertEquals(15,authorTypeList.size(),"Incorrect size of list of authors");
    }
}
