package ua.com.epam.negative;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ua.com.epam.entities.AuthorType;
import ua.com.epam.entities.CreateAuthorRequest;
import ua.com.epam.entities.DeleteAuthorRequest;
import ua.com.epam.services.AuthorService;

public class CreateAuthorWIthExistedId {
    AuthorType authorEntity;
    AuthorService authorService;
    CreateAuthorRequest postRequest;
    long idOfExistedAuthor;

    @BeforeTest
    public void setUp() {
        authorService = new AuthorService();
        authorEntity = authorService.createAuthorEntity();
        postRequest = new CreateAuthorRequest();
        postRequest.setAuthor(authorEntity);
        AuthorType authorResponse = authorService.createAuthor(postRequest);
        Assert.assertEquals(authorResponse.getAuthorId(),authorEntity.getAuthorId(),"ID's for authors are " +
                "not correct");
        idOfExistedAuthor = authorEntity.getAuthorId();

    }
    @Test
    public void createAuthorWithExistedId () {
        String correctMessage = "Author with id " + idOfExistedAuthor +" already exists";
        postRequest.setAuthor(authorService.createAuthorEntity(idOfExistedAuthor));
        try {
            authorService.createAuthor(postRequest);
        }
        catch (Exception ex) {
            Assert.assertTrue(ex.getMessage().contains(correctMessage));
        }
    }

    @AfterTest
    public void freeResources (){
        DeleteAuthorRequest deleteAuthor = new DeleteAuthorRequest();
        deleteAuthor.setAuthorId(idOfExistedAuthor);
        String deleteResponse = authorService.deleteAuthor(deleteAuthor);
        Assert.assertEquals(deleteResponse, "Successfully deleted author " + idOfExistedAuthor,
                "Author cannot be deleted");
    }
}
