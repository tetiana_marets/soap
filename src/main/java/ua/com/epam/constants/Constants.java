package ua.com.epam.constants;

import java.math.BigInteger;

public class Constants {
    public static final String BASE_URI = "http://127.0.0.1:8080/ws/library.wsdl";
    public static final String orderType = "asc";
    public static final BigInteger page = BigInteger.ONE;
    public static final Boolean pagination = true;
    public static final int size = 15;
}
