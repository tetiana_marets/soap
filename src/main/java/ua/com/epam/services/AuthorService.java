package ua.com.epam.services;

import com.github.javafaker.Faker;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import ua.com.epam.entities.*;

import java.math.BigInteger;
import java.util.List;

public class AuthorService {
    private static final Logger log = LogManager.getLogger(AuthorService.class);
    LibraryPortService libraryPortService;
    LibraryPort libraryPort;
    private Faker faker;

    public AuthorService(){
        libraryPortService = new LibraryPortService();
        libraryPort = libraryPortService.getLibraryPortSoap11();
        faker = new Faker();
    }

    public AuthorType createAuthorEntity (){
        AuthorType authorEntity = new AuthorType();
        Authors authors = getAllAuthors();
        long authorId = getLastAuthorId(authors.getAuthor()) + 1;
        authorEntity.setAuthorId(authorId);
        AuthorType.AuthorName authorName = new AuthorType.AuthorName();
        authorName.setFirst(faker.name().firstName());
        authorName.setSecond(faker.name().lastName());
        authorEntity.setAuthorName(authorName);
        AuthorType.Birth authorBirthday = new AuthorType.Birth();
        authorBirthday.setCity(faker.address().city());
        authorBirthday.setCountry(faker.address().country());
        authorBirthday.setDate("1987-08-06");
        authorEntity.setBirth(authorBirthday);
        authorEntity.setNationality(faker.address().countryCode());
        authorEntity.setAuthorDescription(faker.gameOfThrones().dragon());
        return authorEntity;
    }

    public AuthorType createAuthorEntity (long authorId){
        AuthorType authorEntity = new AuthorType();
        authorEntity.setAuthorId(authorId);
        AuthorType.AuthorName authorName = new AuthorType.AuthorName();
        authorName.setFirst(faker.name().firstName());
        authorName.setSecond(faker.name().lastName());
        authorEntity.setAuthorName(authorName);
        AuthorType.Birth authorBirthday = new AuthorType.Birth();
        authorBirthday.setCity(faker.address().city());
        authorBirthday.setCountry(faker.address().country());
        authorBirthday.setDate("1987-08-06");
        authorEntity.setBirth(authorBirthday);
        authorEntity.setNationality(faker.address().countryCode());
        authorEntity.setAuthorDescription(faker.gameOfThrones().dragon());
        return authorEntity;
    }

    private long getLastAuthorId(List<AuthorType> authorList) {
        return authorList.get(authorList.size()-1).getAuthorId();
    }

    public Authors getAllAuthors () {
        GetAuthorsRequest getAuthors = new GetAuthorsRequest();

        SearchParamsWithPagination params = new SearchParamsWithPagination();
        params.setOrderType("asc");
        params.setPage(BigInteger.ONE);
        params.setSize(100);
        params.setPagination(false);
        getAuthors.setSearch(params);


        GetAuthorsResponse response = libraryPort.getAuthors(getAuthors);
        log.info(response);
        return response.getAuthors();
    }

    public AuthorType createAuthor (CreateAuthorRequest request){
        CreateAuthorResponse response = libraryPort.createAuthor(request);
        log.info(response);
        return response.getAuthor();
    }

    public String deleteAuthor (DeleteAuthorRequest request){
        DeleteAuthorResponse response = libraryPort.deleteAuthor(request);
        log.info(response);
        return response.getStatus();
    }

    public AuthorType getAuthor (GetAuthorRequest request){
        GetAuthorResponse response = libraryPort.getAuthor(request);
        log.info(response);
        return response.getAuthor();
    }
}
